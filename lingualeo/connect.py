import json
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
from http.cookiejar import CookieJar


class Lingualeo:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.cj = CookieJar()

    def auth(self):
        url = "https://lingualeo.com/ru/uauth/dispatch"
        values = {"email": self.email, "password": self.password, "type":"login", "source": "landing"}
        return self.get_content(url, values)

    def get_page(self, page_number):
        url = 'https://lingualeo.com/ru/userdict/json'
        values = {'filter': 'all', 'page': page_number}
        return self.get_content(url, values)['userdict3']

    def get_content(self, url, values):
        data = urllib.parse.urlencode(values).encode("utf-8")
        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.cj))
        req = opener.open(url, data)
        return json.loads(req.read())

    def get_user_dicts(self):
        url = 'https://lingualeo.com/userdict3/getWordSets?_=1'
        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.cj))
        req = opener.open(url, None)
        word_set = json.loads(req.read())
        group_dict = dict()
        for item in word_set['result']:
            group_dict[item['id']] = item['name']
        return group_dict

    def get_all_words(self, missed, last_word, user_dict):
        """
        The JSON consists of list "userdict3" on each page
        Inside of each userdict there is a list of periods with names
        such as "October 2015". And inside of them lay our words.
        Returns: type == list of dictionaries
        """
        words = []
        have_periods = True
        page_number = 1
        while have_periods:
            periods = self.get_page(page_number)
            if len(periods) > 0:
                for period in periods:
                    # if missed can be 2(True) or 0(False)
                    # i have no idea why True doesn't output
                    if missed == 2 and last_word:
                        if period['words']:
                            for period_word in period['words']:
                                if period_word['word_value'] == last_word:
                                    self.add_dict(words, user_dict)
                                    return words
                    words += period['words']
            else:
                have_periods = False
            page_number += 1
        self.add_dict(words, user_dict)
        return words

    def add_dict(self, words, user_dict):
        for word in words:
            word["user_dict"] = user_dict
